---- imgui type convertion functions

-- requires BUFFER_SIZE definition

--- converts lua types to mimgui ctypes
-- lua: boolean                       -> imgui: bool[1]
-- lua: string                        -> imgui: char[BUFFER_SIZE]
-- lua 5.1 doesn't differentiate int and float types so:
-- lua: number, has 'f_' at start     -> imgui: float[1]
-- lua: number, has 'i_' at start     -> imgui: int[1]
-- lua: number, has 'color_' at start -> imgui: float[4]
function imgui.to(table_name, ignored_section)
   for section_name, _ in pairs(_G[table_name]) do
      if section_name ~= ignored_section then
         local settings = _G[table_name][section_name]
         for key, value in pairs(settings) do
            local data_type = type(value)
            if data_type == "boolean" then 
               settings[key] = imgui.new.bool(value)
            end
            if data_type == "string" then
               settings[key] = imgui.new.char[BUFFER_SIZE](u8(value))
            end
            if data_type == "number" then
               if key:find("f_") then
                  settings[key] = imgui.new.float(value)
               end
               if key:find("i_") then
                  settings[key] = imgui.new.int(value)
               end
               if key:find("color") then
                  local imvec = imgui.ColorConvertU32ToFloat4(value)
                  settings[key] = imgui.new.float[4](imvec.z, imvec.y, imvec.x, imvec.w)
               end
            end
         end
         _G[table_name][section_name] = settings
      end
   end
end

function imgui.from(table_name, ignored_section)
   for section_name, _ in pairs(_G[table_name]) do
      if section_name ~= ignored_section then 
         local settings = _G[table_name][section_name]
         for key, value in pairs(settings) do
            if type(value) == "cdata" then
               local buf_size = tostring(ffi.typeof(value)):match("ctype<%S+%s%[(%d+)%]>")
               if buf_size then
                  -- ctype<T>[uint] matched => string, bool or number or float[4]
                  if tonumber(buf_size) == 256 then
                     settings[key] = u8:decode(ffi.string(value))
                  end
                  if tonumber(buf_size) == 4 then
                     local a, r, g, b = value[3] * 255, value[0] * 255, value[1] * 255, value[2] * 255
                     settings[key] = join_argb(a, r, g, b)
                  end
                  if tonumber(buf_size) == 1 then
                     settings[key] = value[0]
                  end
               end 
               -- expect ctype<struct ImVec4>
               --settings[key] = imgui.ColorConvertFloat4ToU32(value)
            else
               assert(false, "Key does not exist or is not cdata")
            end
         end
         _G[table_name][section_name] = settings
      end
   end
end