function join_argb(a, r, g, b)
   local u32 = b
   u32 = bit.bor(u32, bit.lshift(g, 0x8))
   u32 = bit.bor(u32, bit.lshift(r, 0x10))
   u32 = bit.bor(u32, bit.lshift(a, 0x18))

   return u32
end
 
function argb_to_rgb(u32)
   return bit.band(u32, 0xFFFFFF)
end
