-- clickable link
function imgui.Link(link, name, color, color_hovered)
   local size = imgui.CalcTextSize(name)
   local cursor_pos = imgui.GetCursorPos()
   local result_btn = imgui.InvisibleButton("##" .. link .. name, size)

   if result_btn then
      os.execute("explorer ".. link)
   end

   imgui.SetCursorPos(cursor_pos)
   if imgui.IsItemHovered() then
      imgui.TextColored(color_hovered, name)
   else
      imgui.TextColored(color, name)
   end
   
   return result_btn
end

function imgui.FontSelect(tbl, old_font)
   local font = old_font
   if imgui.InputText("Face", tbl.font, BUFFER_SIZE) then
      local font_face = u8:decode(ffi.string(tbl.font))
      font = renderCreateFont(font_face, tbl.i_size[0], tbl.i_flag[0])
   end

   if imgui.InputInt("Size", tbl.i_size) then
      local font_face = u8:decode(ffi.string(tbl.font))
      tbl.i_size[0] = tbl.i_size[0] % 50
      font = renderCreateFont(font_face, tbl.i_size[0], tbl.i_flag[0])
   end

   if imgui.InputInt("Style", tbl.i_flag) then
      local font_face = u8:decode(ffi.string(tbl.font))
      tbl.i_flag[0] = tbl.i_flag[0] % 13
      font = renderCreateFont(font_face, tbl.i_size[0], tbl.i_flag[0])
   end
   return font
end

function imgui.SelectBox(arr, btn_size_x, btn_size_y, spacing, is_horizontal, starting_idx)
   local start = imgui.ImVec2(style.child_spacing, style.child_spacing)
   local child_size_x = is_horizontal and btn_size_x * #arr + (#arr - 1) * spacing + style.child_spacing * 2 or btn_size_x + style.child_spacing * 2
   local child_size_y = is_horizontal and btn_size_y + style.child_spacing * 2 or btn_size_y * #arr + (#arr - 1) * spacing + style.child_spacing * 2
   local child_size = imgui.ImVec2(child_size_x, child_size_y)
   imgui.BeginChild("##selectbox" .. tostring(starting_idx), child_size, true, style.child_flags)
   do
      local btn_size = imgui.ImVec2(btn_size_x, btn_size_y)
      imgui.SetCursorPos(start)
      for idx, text in ipairs(arr) do
         local cur_clr = (panel_idx == idx - 1 + starting_idx) and style.btn_color.selected or style.btn_color.default
         if imgui.CustomButton(text, cur_clr, style.btn_color.hovered, style.btn_color.pressed, btn_size) then
            panel_idx = (idx - 1 + starting_idx)
         end
         local next_pos_x = is_horizontal and (start.x + idx * btn_size_x + idx * spacing) or start.x
         local next_pos_y = (not is_horizontal) and  (start.y + idx * btn_size_y + idx * spacing) or start.y
         imgui.SetCursorPos(imgui.ImVec2(next_pos_x, next_pos_y))
      end
   end
   imgui.EndChild()
   return child_size
end

imgui.Switch = function(bool, size)
   local start_style = bool and style.on or style.off
   return imgui.CustomButton(ti("power"), start_style.default, start_style.hovered, start_style.pressed, size)
end

imgui.CustomButton = function(name, color, color_hovered, color_pressed, size)
   local col = imgui.Col
   imgui.PushStyleColor(col.Button, color)
   imgui.PushStyleColor(col.ButtonHovered, color_hovered)
   imgui.PushStyleColor(col.ButtonActive, color_pressed)
   local result = imgui.Button(name, size)
   imgui.PopStyleColor(3)
   return result
end