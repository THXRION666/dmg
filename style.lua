local style = {}
local imgui = require("mimgui")
local vec4 = imgui.ImVec4

style.win_flags = imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.AlwaysAutoResize
style.child_flags = imgui.WindowFlags.NoScrollbar

style.white = vec4(1, 1, 1, 1)
style.orange = vec4(1, 0.32, 0, 1)
style.blue = vec4(0.32, 0.64, 1, 1)
style.gray = vec4(0.5, 0.5, 0.5, 1)


style.on = {
   default  = imgui.ImVec4(0.14, 0.5, 0.14, 1.00), 
   hovered  = imgui.ImVec4(0.15, 0.59, 0.18, 0.5), 
   pressed  = imgui.ImVec4(0.15, 0.59, 0.18, 0.4)
}

style.off = {
	default  = imgui.ImVec4(0.5, 0.14, 0.14, 1.00), 
	hovered  = imgui.ImVec4(1, 0.19, 0.19, 0.3), 
	pressed  = imgui.ImVec4(1, 0.19, 0.19, 0.2)
}

--- Button styling
style.btn_color = {
	default  = imgui.ImVec4(0.24, 0.24, 0.24, 1.00),
	selected = imgui.ImVec4(0.40, 0.40, 0.40, 1.00),
	hovered  = imgui.ImVec4(0.14, 0.14, 0.14, 1.00), 
   pressed  = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
}

style.start           = imgui.ImVec2(8, 25)  -- top left corner of the menu
style.spacing         = 2                    -- spacing between elements of menu
style.child_spacing   = 5                    -- child window padding
style.btn_size        = imgui.ImVec2(70, 26) -- default button size

-- imgui window style
style.apply_style = function()
   -- thx man with a moon profile picture
   imgui.SwitchContext()
   local _style = imgui.GetStyle()
   _style.FrameRounding = 4.0
	local colors, col = _style.Colors, imgui.Col


   
   colors[col.Text]                    = vec4(1.00, 1.00, 1.00, 1.00)
   colors[col.TextDisabled]            = vec4(1.00, 1.00, 1.00, 0.20)
   colors[col.PopupBg]                 = vec4(0.90, 0.90, 0.90, 1.00)
   colors[col.Border]                  = vec4(0.32, 0.32, 0.32, 0.00)
   colors[col.SliderGrab]              = vec4(0.90, 0.90, 0.90, 1.00)
   colors[col.SliderGrabActive]        = vec4(0.70, 0.70, 0.70, 1.00)
   colors[col.BorderShadow]            = vec4(0.00, 0.00, 0.00, 0.00)
   colors[col.ScrollbarBg]             = vec4(0.60, 0.60, 0.60, 0.90)
   colors[col.ScrollbarGrab]           = vec4(0.90, 0.90, 0.90, 1.00)
   colors[col.ScrollbarGrabHovered]    = vec4(0.80, 0.80, 0.80, 1.00)
   colors[col.ScrollbarGrabActive]     = vec4(0.70, 0.70, 0.70, 1.00)
   colors[col.FrameBg]                 = vec4(0.20, 0.20, 0.20, 1.00)
   colors[col.FrameBgHovered]          = vec4(0.20, 0.20, 0.20, 0.80)
   colors[col.FrameBgActive]           = vec4(0.20, 0.20, 0.20, 0.60)
   colors[col.CheckMark]               = vec4(1.00, 1.00, 1.00, 1.00)
   colors[col.Button]                  = vec4(0.20, 0.20, 0.20, 1.00)
   colors[col.ButtonHovered]           = vec4(0.15, 0.15, 0.15, 1.00)
   colors[col.ButtonActive]            = vec4(0.10, 0.10, 0.10, 1.00)
   colors[col.TextSelectedBg]          = vec4(0.80, 0.80, 0.80, 0.80)
   colors[col.TitleBg]                 = vec4(0.20, 0.20, 0.20, 1.00)
   colors[col.TitleBgActive]           = vec4(0.20, 0.20, 0.20, 1.00)
   colors[col.Header]                  = vec4(0.20, 0.20, 0.20, 1.00)
   colors[col.HeaderHovered]           = vec4(0.30, 0.30, 0.30, 1.00)
   colors[col.HeaderActive]            = vec4(0.40, 0.40, 0.40, 1.00)
end

return style