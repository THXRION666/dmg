function get_default_config()
   return {
      general = {
         on = true
      },
      skin = {
         i_on = 2, -- 1 = off, 2 = font, 3 = icon
         font = "Arial",
         i_size = 8,
         i_flag = 13,
         i_icon_size = 12,
         i_time = 4,
         color = 0xFFFB4343
      },
      info = {
         on = true,
         enemy_color = true,
         give_down = true,
         i_give_x = 200,
         i_give_y = 200,
         i_give_align = 2,
         take_down = false,
         i_take_x = 300,
         i_take_y = 200,
         i_take_align = 2,
         i_indent = 2,
         i_lines = 3,
         font = "Arial",
         i_size = 8,
         i_flag = 13,
         i_time = 4,
         give_color = 0xFF00FF00,
         take_color = 0xFFFF0000,
         mask = "Stranger"
      },
      sound = {
         give = true,
         f_give = 0.1,
         take = true,
         f_take = 0.1
      }
   }
end