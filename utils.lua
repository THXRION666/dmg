relative_pos = {
   x = function(num)
      local res_x, res_y = getScreenResolution()
      return num * 640 / res_x
   end,
   y = function(num)
      local res_x, res_y = getScreenResolution()
      return num * 480 / res_y
   end
}

screen_pos = {
   x = function(num)
      local res_x, res_y = getScreenResolution()
      return num * res_x / 640
   end,
   y = function(num)
      local res_x, res_y = getScreenResolution()
      return num * res_y / 480
   end
}

font_metrics = {
   x = function(font, text)
      return relative_pos.x(renderGetFontDrawTextLength(font, text))
   end,
   y = function(font)
      return relative_pos.y(renderGetFontDrawHeight(font))
   end
}

-- prints message to SAMP Chat
function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, script.this.name, msg), -1)
end

function get_align(var)
   local mult = { 0, 0.5, 1 }
   return mult[var[0]]
end

function clamp(var, min, max)
   if var < min then 
      return min 
   end
   if var > max then 
      return max 
   end
   return var
end

function is_nt_visible(id)
   local struct = memory.getint32(sampGetPlayerStructPtr(id), true)
   local mask = getStructElement(struct, 179, 2, false)
   return mask ~= 0
end

function get_player_idx(some_list, player_id)
   for index, data in ipairs(some_list) do
      if data.player_id == player_id then 
         return index 
      end
   end
end

function get_damage(weapon, damage)
   local arms = {
      [22] = 13, [23] = 13, [24] = 46,
      [25] = -1, [26] = -1, [27] = -1,
      [28] = 7, [29] = 9, [30] = 16, [31] = 12, [32] = 7,
      [33] = 24, [34] = 46,
      [38] = 23
   }
   local amount = arms[weapon]
   return amount == -1 and math.floor(damage) or amount
end

function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end