imgui.OnInitialize(function()
   local config = imgui.ImFontConfig()
   config.MergeMode = true
   config.PixelSnapH = true
   local ranges = imgui.new.ImWchar[3](ti.min_range, ti.max_range, 0)
   imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(ti.get_font_data_base85(), 15, config, ranges)

   style.apply_style()
   imgui.GetIO().IniFilename = nil
   -- so it does not create any config files
end)

local logo = script.this.name .. " | Author: THERION"

imgui.OnFrame(function() return win_state[0] end, function(player)
   imgui.Begin(logo, win_state, style.win_flags)
      do -- links
         imgui.Link(script.this.url, ti("brand-gitlab"), style.white, style.orange)
         
         imgui.SameLine((imgui.GetWindowSize().x - imgui.CalcTextSize(logo).x) / 2)
         imgui.Text(logo)
      end

      local select_size = style.spacing + 2 * (style.btn_size.x + style.child_spacing)

      imgui.BeginChild("on", imgui.ImVec2(select_size, style.btn_size.y + style.child_spacing * 2), true, style.child_flags)
         imgui.SetCursorPos(imgui.ImVec2(style.child_spacing , style.child_spacing))
         
         if imgui.Switch(ini.general.on, style.btn_size) then
            ini.general.on = not ini.general.on
         end

         imgui.SetCursorPos(imgui.ImVec2(style.child_spacing + style.btn_size.x + style.spacing, style.child_spacing))
         
         if imgui.CustomButton(ti("bookmark"), style.btn_color.default, style.btn_color.hovered, style.btn_color.pressed, style.btn_size) then
            imgui.from("ini", "general")
            inicfg.save(ini, script.this.name .. ".ini") 
            imgui.to("ini", "general")
         end
      imgui.EndChild()

      imgui.SameLine(7 + select_size)

      local box = imgui.SelectBox({ti("crosshair"), ti("list"), ti("bell")}, style.btn_size.x, style.btn_size.y, 1, true, 1)
      select_size = select_size + box.x

      imgui.SetCursorPos(imgui.ImVec2(8, box.y + 25))

      imgui.BeginChild("settings", imgui.ImVec2(select_size - 1, 350), true, style.child_flags)
         if panel_idx == 1 then
            imgui.Text("Display on skin:")
            for idx, string in ipairs({"Nothing", "Damage", "Icon"}) do
               imgui.RadioButtonIntPtr(string, ini.skin.i_on, idx)
               imgui.SameLine()
            end
            imgui.Text("")

            if ini.skin.i_on[0] == 2 then
               if imgui.CollapsingHeader("Font") then
                  skin_font = imgui.FontSelect(ini.info, skin_font)
               end
            end

            if ini.skin.i_on[0] == 3 then
               imgui.SliderInt("Icon size", ini.skin.i_icon_size, 0, 64)
            end

            if ini.skin.i_on[0] ~= 1 then
               imgui.SliderInt("Fade time", ini.skin.i_time, 0, 20)
               imgui.ColorEdit4("Color", ini.skin.color)
            end

            if os.clock() > last_demo_hit_time + ini.skin.i_time[0] then
               last_demo_hit_time = os.clock()
               
               do
                  local x, y, z = getCharCoordinates(PLAYER_PED)
                  local vec = {x = math.random(), y = math.random(), z = math.random()}
                  x, y, z = x + vec.x, y + vec.y, z + vec.z
                  lua_thread.create(draw_onskin_hit, math.floor(math.random() * 46) , x, y, z)
               end
            end
         end
         if panel_idx == 2 then
            imgui.Checkbox("Display dealt damage info", ini.info.on)
            imgui.Checkbox("Copy enemy color", ini.info.enemy_color)
            imgui.SliderInt("Fade time", ini.info.i_time, 0, 20)
            imgui.SliderInt("Indent", ini.info.i_indent, 0, 10)
            if imgui.SliderInt("Lines", ini.info.i_lines, 0, 10) then
               if ini.info.i_lines[0] < #give_list then
                  for i = ini.info.i_lines[0] + 1, #give_list do
                     table.remove(give_list, i)
                  end
               end
               if ini.info.i_lines[0] < #take_list then
                  for i = ini.info.i_lines[0] + 1, #take_list do
                     table.remove(take_list, i)
                  end
               end
            end
            imgui.InputText("If player wears mask:", ini.info.mask, BUFFER_SIZE)
            local choose_size = imgui.ImVec2(240, 20)
            local item_width = 115

            local function display_demo_info()
               for _, keyword in ipairs({"give", "take"}) do
                  local tbl = ini.info
                  local x_pos = "i_" .. keyword .. "_x"
                  local y_pos = "i_" .. keyword .. "_y"
                  local align = "i_" .. keyword .. "_align"
                  local col = keyword .. "_color"
            
                  local msg = "(-100) Purple Dildo {FB4343}therion[666]"
                  local pos = {
                     x = tbl[x_pos][0] - font_metrics.x(info_font, msg) * get_align(tbl[align]),
                     y = tbl[y_pos][0]
                  }
                  pos.x = pos.x * res_x / 640
                  pos.y = pos.y * res_y / 480
            
                  local color = tbl[col]
            
                  local a, r, g, b = color[3] * 255, color[0] * 255, color[1] * 255, color[2] * 255
                  color = join_argb(a, r, g, b)
            
                  renderFontDrawText(info_font, msg, pos.x, pos.y, color)
               end
            end

            local function getinfo(tbl, keyword)
               if imgui.RadioButtonBool("Top to bottom##" .. keyword, tbl[keyword .. "_down"][0]) then
                  tbl[keyword .. "_down"][0] = not tbl[keyword .. "_down"][0]
               end

               local x_pos = "i_" .. keyword .. "_x"
               local y_pos = "i_" .. keyword .. "_y"
               local align = "i_" .. keyword .. "_align"
               local col = keyword .. "_color"
               imgui.PushItemWidth(item_width)
               do
                  if imgui.InputInt("##x" .. keyword, tbl[x_pos]) then
                     tbl[x_pos][0] = tbl[x_pos][0] % 640
                  end
                  imgui.SameLine()
                  if imgui.InputInt("##y" .. keyword, tbl[y_pos]) then
                     tbl[y_pos][0] = tbl[y_pos][0] % 480
                  end
               end
               imgui.PopItemWidth()

               imgui.Text("Align:")
               for idx, string in ipairs({"Left", "Center", "Right"}) do
                  imgui.SameLine()
                  imgui.RadioButtonIntPtr(string .. "##" .. align, tbl[align], idx)
               end

               if imgui.Button(ti("refresh") .. " Choose##" .. keyword, choose_size) then
                  lua_thread.create(function()
                     win_state[0] = false
                     sampSetCursorMode(3)

                     while true do wait(0)
                        tbl[x_pos][0], tbl[y_pos][0] = getCursorPos()
                        tbl[x_pos][0] = tbl[x_pos][0] / res_x * 640
                        tbl[y_pos][0] = tbl[y_pos][0] / res_y * 480

                        display_demo_info()
                        if isKeyJustPressed(0x1) then
                           sampSetCursorMode(0)
                           win_state[0] = true
                           return
                        end
                     end
                  end)
               end
               imgui.ColorEdit4("Color##" .. keyword, tbl[col])
            end

            if imgui.CollapsingHeader("Victim list") then
               getinfo(ini.info, "give")
            end

            if imgui.CollapsingHeader("Attacker list") then
               getinfo(ini.info, "take")
            end

            if imgui.CollapsingHeader("Font") then
               info_font = imgui.FontSelect(ini.info, info_font)
            end

            display_demo_info()
         end
         if panel_idx == 3 then
            imgui.Checkbox("Play sound on hit", ini.sound.give)
            if imgui.SliderFloat(ti("volume") .. "##give", ini.sound.f_give, 0, 4) then
               bass.BASS_ChannelSetAttribute(give_sound, BASS_ATTRIB_VOL, ini.sound.f_give[0])
            end
            if imgui.Button(ti("headphones") .. " Listen##give") then
               bass.BASS_ChannelPlay(give_sound, false)
            end
            imgui.Checkbox("Play sound when attacked", ini.sound.take)
            if imgui.SliderFloat(ti("volume") .. "##take", ini.sound.f_take, 0, 4) then
               bass.BASS_ChannelSetAttribute(take_sound, BASS_ATTRIB_VOL, ini.sound.f_take[0])
            end
            if imgui.Button(ti("headphones") .. " Listen##take") then
               bass.BASS_ChannelPlay(take_sound, false)
            end
         end
      imgui.EndChild()
   imgui.End()
end)