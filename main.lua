script_name("DMG")
script_author("THERION")
script_url("https://gitlab.com/modarnya/moonloader")
script_description("Damage notification")
script_version(4)

-- lib

imgui = require("mimgui")
ti = require("tabler_icons")
inicfg = require("inicfg")
ev = require("samp.events")
ffi = require("ffi")
memory = require("memory")
bass = require("bass")
encoding = require("encoding")
clr = require("color_works")
weapons = require("game.weapons")
encoding.default = "CP1251"
u8 = encoding.UTF8

-- modules
require("const")
require("utils")
require("color")

require("config")
require("serialization")

style = require("style")
require("widgets")
require("menu")
--

-- assets
icon = nil
skin_font, info_font = nil
give_sound, take_sound = nil

-- state
ini = inicfg.load(get_default_config(), script.this.name .. ".ini")
last_weap = nil
last_victim = -1
last_time = nil
aiming_state = false
last_demo_hit_time = 0
win_state = imgui.new.bool(false)
panel_idx = 1
give_list = {
   {text = "(-100) M4 {6C3232}therion[666]", current_damage = 100, remove_time = 1},
   {text = "(-100) Country rifle {FFFFFF}dolbit_otlichno[228]", current_damage = 100, remove_time = 1}
}
take_list = {
   {text = "(-100) M4 {6C3232}therion[666]", current_damage = 100, remove_time = 1},
   {text = "(-100) Country rifle {FFFFFF}dolbit_otlichno[228]", current_damage = 100, remove_time = 1}
}

local function damage_info_tostring(player_id, nick, damage, weapon)
   local text = ""
   if is_nt_visible(player_id) then
      if ini.info.enemy_color[0] then
         local player_color = argb_to_rgb(sampGetPlayerColor(player_id))
         text = string.format("(-%d) %s {%06X}%s[%d]", damage, weapon, player_color, nick, player_id)
      else
         text = string.format("(-%d) %s %s[%d] ", damage, weapon, nick, player_id)
      end
   else
      text = string.format("(-%d) %s %s", damage, weapon, u8:decode(ffi.string(ini.info.mask)))
   end
   return text
end

local function draw_onskin_hit(damage, x, y, z)
   local fade_time = os.clock() + ini.skin.i_time[0]

   while fade_time >= os.clock() do wait(0)
      if isPointOnScreen(x, y, z, 1) then
         local screen_x, screen_y = convert3DCoordsToScreen(x, y, z)
         
         local color = ini.skin.color
         do
            local a, r, g, b = color[3] * 255, color[0] * 255, color[1] * 255, color[2] * 255
            color = join_argb(a, r, g, b)
         end
         
         if ini.skin.i_on[0] == 2 then
            local pos = {
               x = screen_x - font_metrics.x(skin_font, damage) / 2, 
               y = screen_y - font_metrics.y(skin_font)
            }
            renderFontDrawText(skin_font, damage, pos.x, pos.y, color)
         end
         if ini.skin.i_on[0] == 3 then
            local pos = {
               x = screen_x - ini.skin.i_icon_size[0] / 2, 
               y = screen_y - ini.skin.i_icon_size[0] / 2
            }
            renderDrawTexture(icon, pos.x, pos.y, ini.skin.i_icon_size[0], ini.skin.i_icon_size[0], 0, color)
         end
      end
   end
end

local function register_take_damage(player_id, damage, weapon)
   if not isGamePaused() and isCharOnScreen(PLAYER_PED) then
      if ini.info.on[0] then
         local match_case = get_player_idx(take_list, player_id)
         local nick = player_id == -1 and "" or sampGetPlayerNickname(player_id)

         if match_case then
            local summed_damage = take_list[match_case].current_damage + damage
            take_list[match_case] = {
               player_id = player_id,
               text = player_id ~= -1 and damage_info_tostring(player_id, nick, summed_damage, weapon) 
                  or string.format("(-%d) Server", summed_damage),
               current_damage = summed_damage,
               remove_time = os.clock() + ini.info.i_time[0]
            }
         else
            local var = {
               player_id = player_id and player_id or -1,
               text = player_id ~= -1 and damage_info_tostring(player_id, nick, damage, weapon)
                  or string.format("(-%d) Server", damage),
               current_damage =  damage,
               remove_time = os.clock() + ini.info.i_time[0]
            }
            if (#take_list >= ini.info.i_lines[0]) then
               take_list[#take_list + 1] = var
               for i = 1, #take_list - 1 do 
                  take_list[i] = take_list[i + 1] 
               end
               table.remove(take_list)
            else
               table.insert(take_list, var)
            end
         end
         last_victim = -1
         last_weap = nil
         last_time = nil
      end
      if ini.sound.take[0] then
         bass.BASS_ChannelPlay(take_sound, true)
      end
   end
end

local function onSendAimSync(data)
	aiming_state = data.camMode == 53 or data.camMode == 7 or memory.getint8(0xB6FC70) == 1
end

local function onSendBulletSync(data)
   last_x, last_y, last_z = data.target.x, data.target.y, data.target.z
end

local function onBulletSync(id, data)
   local streamed, local_id = sampGetPlayerIdByCharHandle(PLAYER_PED)
   if streamed and data.targetId == local_id then
      last_victim = id
      last_weap = data.weaponId
      last_time = os.clock()
   end
end

local function onSendGiveDamage(player_id, damage, weapon, _)
   if aiming_state and firearms[weapon] then
      damage = damage >= 5 and get_damage(weapon, damage) or math.floor(damage)

      if ini.info.on[0] then
         local nick = sampGetPlayerNickname(player_id)
         local weap = weapons.names[weapon]
         local match_case = get_player_idx(give_list, player_id)
         
         if match_case then
            local summed_damage = give_list[match_case].current_damage + damage
            give_list[match_case] = {
               player_id = player_id,
               text = damage_info_tostring(player_id, nick, summed_damage, weap),
               current_damage = summed_damage,
               remove_time = os.clock() + ini.info.i_time[0]
            }
         else
            local var = {
               player_id = player_id,
               text = damage_info_tostring(player_id, nick, damage, weap),
               current_damage = damage,
               remove_time = os.clock() + ini.info.i_time[0]
            }

            if (#give_list >= ini.info.i_lines[0]) then
               give_list[#give_list + 1] = var
               for i = 1, #give_list - 1 do 
                  give_list[i] = give_list[i + 1] 
               end
               table.remove(give_list)
            else
               table.insert(give_list, var)
            end
         end
      end
      if ini.sound.give[0] then
         bass.BASS_ChannelPlay(give_sound, true)
      end
      if ini.skin.i_on[0] ~= 1 then
         lua_thread.create(draw_onskin_hit, damage, last_x, last_y, last_z)
      end
   end
end

local function onSetPlayerHealth(health)
   local player_id = last_victim
   local damage = clamp(getCharHealth(PLAYER_PED), 0, 160) - health
   local weapon = last_weap and weapons.names[last_weap] or 0
   if damage > (last_victim == -1 and 1 or 0) then
      register_take_damage(player_id, damage, weapon)
   end
end

local function onSetPlayerArmour(armor)
   local player_id = last_victim
   local damage = clamp(getCharArmour(PLAYER_PED), 0, 100) - armor
   local weapon = last_weap and weapons.names[last_weap] or 0
   if damage > (last_victim == -1 and 1 or 0) then
      register_take_damage(player_id, damage, weapon)
   end
end

local function onBeforeInit()
   local config_path = script.this.name .. ".ini"
   
   if not doesFileExist(config_path) then 
      inicfg.save(ini, config_path)
   end
end

local function onInit()
   local ip, _ = sampGetCurrentServerAddress()
   if not is_in_array(GALAXY_IP, ip) then
      script.this:unload()
      return
   end

   give_sound = bass.BASS_StreamCreateFile(false, "moonloader\\resource\\DMG\\give_sounds\\bell.wav", 0, 0, 0)
   bass.BASS_ChannelSetAttribute(give_sound, BASS_ATTRIB_VOL, ini.sound.f_give)

   take_sound = bass.BASS_StreamCreateFile(false, "moonloader\\resource\\DMG\\take_sounds\\bell.mp3", 0, 0, 0)
   bass.BASS_ChannelSetAttribute(take_sound, BASS_ATTRIB_VOL, ini.sound.f_take)

   skin_font = renderCreateFont(ini.skin.font, ini.skin.i_size, ini.skin.i_flag)
   renderFontDrawText(skin_font, "moon 026.5 compatibility", 10000, 10000, -1)
   info_font = renderCreateFont(ini.info.font, ini.info.i_size, ini.info.i_flag)
   renderFontDrawText(info_font, "moon 026.5 compatibility", 10000, 10000, -1)

   icon = renderLoadTextureFromFile("moonloader\\resource\\DMG\\hit_icons\\hit.png")

   imgui.to("ini", "general")

   ev.onSetPlayerHealth = onSetPlayerHealth
   ev.onSetPlayerArmour = onSetPlayerArmour
   ev.onSendGiveDamage = onSendGiveDamage
   ev.onBulletSync = onBulletSync
   ev.onSendBulletSync = onSendBulletSync
   ev.onSendAimSync = onSendAimSync
end

local function onEveryFrame()
   if testCheat("dmg") then
      win_state[0] = not win_state[0]
   end

   local time = os.clock()
   if last_time and last_time + LAST_IMPACT_SAVE > time then
      last_victim = -1
      last_weap = nil
      last_time = nil
   end

   if ini.info.on[0] then
      res_x, res_y = getScreenResolution()
      for index, data in ipairs(give_list) do
         if data.remove_time < time then 
            table.remove(give_list, index)
         else
            local pos = {
               x = ini.info.i_give_x[0] - font_metrics.x(info_font, data.text) * get_align(ini.info.i_give_align),
               y = ini.info.i_give_y[0] + (index - 1) * (font_metrics.y(info_font) + ini.info.i_indent[0]) * (ini.info.give_down[0] and 1 or -1)
            }
            pos.x = pos.x * res_x / 640
            pos.y = pos.y * res_y / 480

            local color = ini.info.give_color
            do
               local a, r, g, b = color[3] * 255, color[0] * 255, color[1] * 255, color[2] * 255
               color = join_argb(a, r, g, b)
            end
            renderFontDrawText(info_font, data.text, pos.x, pos.y, color)
         end
      end
      for index, data in ipairs(take_list) do
         if data.remove_time < time then 
            table.remove(take_list, index)
         else
            local pos = {
               x = ini.info.i_take_x[0] - font_metrics.x(info_font, data.text) * get_align(ini.info.i_take_align),
               y = ini.info.i_take_y[0] + (index - 1) * (font_metrics.y(info_font) + ini.info.i_indent[0]) * (ini.info.take_down[0] and 1 or -1)
            }
            pos.x = pos.x * res_x / 640
            pos.y = pos.y * res_y / 480

            local color = ini.info.take_color

            local a, r, g, b = color[3] * 255, color[0] * 255, color[1] * 255, color[2] * 255
            color = join_argb(a, r, g, b)

            renderFontDrawText(info_font, data.text, pos.x, pos.y, color) 
         end
      end
   end
end

function main()
   onBeforeInit()
   
   while not isSampAvailable() do
      wait(0)
   end

   onInit()

   while true do
      wait(0)
      onEveryFrame()
   end
end
